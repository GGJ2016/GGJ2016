package ggj2016.controller;

import ggj2016.model.*;
import ggj2016.model.Building;
import ggj2016.view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
* Class for managing user interactions
* @author Les Adeptes de Mordak
*/
public class Controller
{
    private int minute;
    private boolean showEventZero;
    private final int minuteToLoop;// the number of loop corresponding to one minute
    private final int nbLoopUpdateModel;
    private final int sleepTime;
    private final EventLauncher eventLauncher;
    private final Model model;
    private final View view;
    
    /**
    * Controller's Constructor
    * @param view      View to manage
    * @param mod       Model to manage
    */
    public Controller(View view, Model mod)
    {
        this.showEventZero = true;
        this.view = view;
        this.model = mod;
        this.minute = 0;
        this.sleepTime = 10;
        this.minuteToLoop = (1000*60)/this.sleepTime;   
        this.eventLauncher = new EventLauncher(this.model.getGame());
        this.nbLoopUpdateModel = 100;
        this.view.getGamePanel().addAllBuildings();
        
        view.addExitListener(new ActionExit());
        view.addUpgradeListener(new ActionUpgrade());
        view.addQuitListener(new ActionExit());
        view.addCreditsListener(new ActionCredits());
        view.addPlayListener(new ActionPlay());
        view.addBackListener(new ActionBack());
        view.addSummonListener(new ActionSummon());
        
        initButtonListeners();
        
        // Menu music
        AssetsManager.get().getMusic().stop();
        AssetsManager.get().playMusic("opening"); // throws the music
        
        this.view.switchToStart();
    }
    
    /**
    * Method for the main loop of the game
    */
    public void run()
    {
        int modelLoopCounter = 0;
        int minuteLoopCounter = 0;
	
        while (!this.view.hasClosed())
	    {
            try
		    {
                Thread.sleep(this.sleepTime);
	
                if((modelLoopCounter >= this.nbLoopUpdateModel) && (this.view.isGameRunning))
			    {				
                    this.updateModel();
                    modelLoopCounter = 0;
			    }	
                this.updateView();
                this.view.repaint();

                if(minuteLoopCounter >= this.minuteToLoop && this.view.isGameRunning )
			    {
                    this.minute++;
                    Log.write("Minute " + this.minute);
                    minuteLoopCounter = 0;

                    // Music
                    AssetsManager.get().getMusic().stop();
                    AssetsManager.get().getSound("roar").play();
				
                    this.view.displayEvent(this.eventLauncher.getEvent(this.minute));
			    }

                if ( this.view.isGameRunning )
			    {
                    if ( this.showEventZero )
				    {
                        this.view.displayEvent(this.eventLauncher.getEvent(0));
                        this.showEventZero = false;
				    }
                    // increment counters
                    modelLoopCounter++;
                    minuteLoopCounter++;
			    }
		    }
            catch (Exception e)
		    {
		    }
	    }
    }

    private void initButtonListeners()
    {
        ArrayList<ggj2016.view.Building> buildings = this.view.getGamePanel().getBuildings();
		
        for(ggj2016.view.Building building : buildings)
	    {
            building.addIncreaseListener(new ActionIncreaseWorkers(building.getBuildingModel()));
            building.addDecreaseListener(new ActionDecreaseWorkers(building.getBuildingModel()));
	    }	
    }

    /**
     * update the model
     */
    private void updateModel()
    {
        // update gold
        {
            Mine m = (Mine) this.model.getGame().getBuilding("Mine");
            this.model.getGame().getGold().add(m.getCurrentIncome());
        }
        // update materials
        {
            Workshop w = (Workshop) this.model.getGame().getBuilding("Workshop");
            this.model.getGame().getMaterials().add(w.getCurrentIncome());
        }
        // update summonPoints
        {
            Graveyard g = (Graveyard) this.model.getGame().getBuilding("Graveyard");
            this.model.getGame().getSummonPoints().add(g.getCurrentIncome());
        }
        // update victory/lose
        {
            if(this.model.getGame().playerWins())
            {
                this.view.switchToWinGame();
                AssetsManager.get().playMusic("victory"); // throws the music
            }
            else if(this.model.getGame().playerLoses(minute))
            {
                this.view.switchToLoseGame();
                AssetsManager.get().playMusic("lose"); // throws the music 	
            }
        }
        // update getPopulation
        {
            Population p =  this.model.getGame().getPopulation();
            double max = ((House)this.model.getGame().getBuilding("House")).getSpace();
            p.increasePopulation(max);         
        }
    }

    /**
    * update the view according to the model
    */
    private void updateView()
    {
        House h = (House) this.model.getGame().getBuilding("House");

        this.view.getResourcePanel().setPopulation(this.model.getGame().getPopulation().getAvailable(), this.model.getGame().getPopulation().getTotal(), h.getSpace());
        this.view.getResourcePanel().setGold(this.model.getGame().getGold().getAmount());
        this.view.getResourcePanel().setMaterials(this.model.getGame().getMaterials().getAmount());
        this.view.getResourcePanel().setSummonPoints(this.model.getGame().getSummonPoints().getAmount());
        this.view.getResourcePanel().setTrust(this.model.getGame().getTrust().getAmount());
        
        for(ggj2016.view.Building b : view.getGamePanel().getBuildings())
	    {
            b.updateTitle();
            b.updateButtons(model.getGame().getPopulation());
	    }
	
        this.view.updateMenuUpgrades();
    }

    /**
    * Exit the game
    * @param cause the reason that make the game quit
    */
    private void stop(String cause)
    {
        Log.write(cause);
        this.view.stop();
        this.view.close();
        System.exit(0);
    }
    
    private class ActionBack implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            Controller.this.view.switchToStart();
        }
    }
    
    private class ActionCredits implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            Controller.this.view.switchToCredits();
        }
    }
    
    /**
    * Action that decrease the number of workers in the building
    */
    private class ActionDecreaseWorkers implements ActionListener
    {
        private final Building building;

        public ActionDecreaseWorkers(Building building)
        {
            this.building = building;
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            building.removeWorkers(1);
            AssetsManager.get().getSound("click-").play();
        }
    }
    
    /**
    * Action that make the game quit
    */
    private class ActionExit implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            Controller.this.stop("Exit button");
        }
    }
    
    /**
    * Action that increase the number of workers in the building
    */
    private class ActionIncreaseWorkers implements ActionListener
    {
        private final Building building;

        public ActionIncreaseWorkers(Building building)
        {
            this.building = building;
        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            building.addWorkers(1);
            AssetsManager.get().getSound("click+").play();
        }
    }
    
    private class ActionPlay implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            Controller.this.view.switchToGame();
        }
    }
    
    /**
    * Action that try to upgrade a building
    */
    private class ActionUpgrade implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            ggj2016.model.Building b = (ggj2016.model.Building) ((MenuItemUpgrade) event.getSource()).getBuildingModel(); // building to upgrade
            Log.write("Trying to upgrade > " + b.getName() + " < from level " + b.getLevel() + " to level " + (b.getLevel() + 1));
      
            int costGold = b.getCostGold();
            int costMaterials = b.getCostMaterials();
            int costSP = b.getCostSP();
      
            Game g = model.getGame();
            int gold = (int)g.getGold().getAmount();
            int materials = (int)g.getMaterials().getAmount();
            int sp = (int)g.getSummonPoints().getAmount();
      
            if((gold >= costGold) && (materials >= costMaterials) && (sp >= costSP))
            {
                if(b.getLevel() == 0)
                {
                    AssetsManager.get().getSound("upgrade").play();
                }
                else
                {
                    AssetsManager.get().getSound("up").play();
                }
                g.payGold(costGold);
                g.payMaterials(costMaterials);
                g.paySP(costSP);
                b.editLevel(1);
            }
            else
            {
                Log.write("Not enough resources to upgrade this building ! (" + b.getName() + ")");
            }  
        }
    }
    
    private class ActionSummon implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            if(model.getGame().getSummonPoints().getAmount() >= 7500)
            {
              model.getGame().summonMordak();
            }
            else
            {
              Log.write("Not enough Summon Points to summon Mordak ! How dare you summon him if you are not worthy !?");
              view.switchToLoseGame();
            }
        }
    }
}
