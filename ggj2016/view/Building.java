package ggj2016.view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.*;

import javax.swing.*;

import ggj2016.model.AssetsManager;
import ggj2016.model.House;
import ggj2016.model.Population;

/**
 * Visual representation of a building
 * @author Les Adeptes de Mordak
 */
public class Building extends JPanel
{
    public static int ICON_WIDTH = 20;
    public static int ICON_HEIGHT = 20;
    private ImageIcon buttonAdd;
    private ImageIcon buttonRemove;
    private final JPanel buttonsPanel;
    private final JButton minusButton;
    private final JButton plusButton;
    private final JLabel imageLabel;
    private final JLabel nameLabel;
    private final ggj2016.model.Building buildingModel;

    /**
     * Constructor
     * @param building      model
     * @param image         the picture representing the building
     */
    public Building(BufferedImage image, ggj2016.model.Building building)
    {
        this.buildingModel = building;
        this.setBackground(new Color(255, 255, 255, 0));
        this.buttonsPanel = new JPanel();
        this.buttonsPanel.setBackground(new Color(0, 0, 0, 0));
        this.setLayout(new BorderLayout());
        // imageLabel
        {
            if (image != null)
            {
                this.imageLabel = new JLabel(new ImageIcon(image));
            }
            else
            {
                this.imageLabel = new JLabel("Building");
            }
            this.add(imageLabel, BorderLayout.CENTER);
        }
        // name
        this.nameLabel = new JLabel();
        this.updateTitle();
        this.nameLabel.setHorizontalAlignment(JLabel.CENTER);
        this.add(this.nameLabel, BorderLayout.SOUTH);
        // buttons
        {
            this.buttonAdd = new ImageIcon(AssetsManager.get().getImage("buttonadd"));
            Image img = this.buttonAdd.getImage() ;
            Image newimg = img.getScaledInstance( ICON_WIDTH, ICON_HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;  
            this.buttonAdd = new ImageIcon(newimg);
            this.plusButton = new JButton(buttonAdd);
            this.plusButton.setContentAreaFilled(false);
            this.plusButton.setBorderPainted(false);
            this.plusButton.setRolloverEnabled(false);
            this.plusButton.setFocusPainted(false);
        
            this.buttonRemove = new ImageIcon(AssetsManager.get().getImage("buttonremove"));
            img = this.buttonRemove.getImage() ;  
            newimg = img.getScaledInstance( ICON_WIDTH, ICON_HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;  
            this.buttonRemove = new ImageIcon(newimg);
            this.minusButton = new JButton(buttonRemove);
            this.minusButton.setContentAreaFilled(false);
            this.minusButton.setBorderPainted(false);
            this.minusButton.setRolloverEnabled(false);
            this.minusButton.setFocusPainted(false);

            if(this.buildingModel.getPosition().getY() < 0)
            {
                // create another disposition for information if the buttons get out of the screen du to the building position
                JPanel panel = new JPanel();
                panel.setLayout(new BorderLayout());
                panel.setBackground(new Color(0, 0, 0, 0));
                panel.add(this.buttonsPanel, BorderLayout.NORTH);
                panel.add(this.nameLabel, BorderLayout.SOUTH);
                this.add(panel, BorderLayout.SOUTH);
            }
            else
            {
                this.add(this.buttonsPanel, BorderLayout.NORTH);
            }
            this.buttonsPanel.add(this.plusButton, BorderLayout.WEST);
            this.buttonsPanel.add(this.minusButton, BorderLayout.EAST);
        }
    }

    public void addIncreaseListener(ActionListener listener)
    {
        this.plusButton.addActionListener(listener);
    }

    public void addDecreaseListener(ActionListener listener)
    {
        this.minusButton.addActionListener(listener);
    }

    public ggj2016.model.Building getBuildingModel()
    {
        return this.buildingModel;
    }
    
    /**
    * Set the title of a building
    * @param name          of the building
    * @param population    working in the building
    * @param level          of the building
    */
    private void setTitle(String name, int population, int maxPop, int level)
    {
        if(this.buildingModel.getClass() == House.class)
        {
          this.nameLabel.setText(name + " lvl " + level);  
        }
        else
        {
          this.nameLabel.setText(name + "(" + population + "/" + maxPop + ")" + " lvl " + level);
        }
        this.imageLabel.setToolTipText(this.nameLabel.getText());
    }

    /**
     * Update JButtons by setting them enabled only when the things they do can actually happen
     * @param population    of the game, to know available workers
     */
    public void updateButtons(Population population)
    {
        if(this.buildingModel.getClass() == House.class)
        {
            if(this.plusButton.isVisible())
            {
                this.plusButton.setVisible(false);
            }
            if(this.minusButton.isVisible())
            {
                this.minusButton.setVisible(false);
            }
        }
        else
        {
            // enable Workers button
            if((this.buildingModel.getCurrentWorkers() == this.buildingModel.getMaxWorkers()) || (population.getAvailable() == 0))
            {
                this.disableAddButton();
            }
            else
            {
                this.enableAddButton();
            }
            // disable workers button
            if(this.buildingModel.getCurrentWorkers() == 0)
            {
                this.disableRemoveButton();
            }
            else
            {
                this.enableRemoveButton();
            }
        }
    }
    
    public void updateTitle()
    {
        if(this.buildingModel.getLevel() == 0)
        {
            this.setVisible(false);
        }
        else
        {
            this.setVisible(true);
            this.setTitle(this.buildingModel.getName(), this.buildingModel.getCurrentWorkers(), this.buildingModel.getMaxWorkers(), this.buildingModel.getLevel());
        }
    }
	
    private void disableAddButton()
    {
        if(this.plusButton.isEnabled())
	    {
            this.plusButton.setEnabled(false);
	    }
    }
    
    private void disableRemoveButton()
    {
        if(this.minusButton.isEnabled())
	    {
            this.minusButton.setEnabled(false);
	    }
    }
	
    private void enableAddButton()
    {
        if(!this.plusButton.isEnabled())
	    {
            this.plusButton.setEnabled(true);
	    }
    }
	
    private void enableRemoveButton()
    {
        if(!this.minusButton.isEnabled())
	    {
            this.minusButton.setEnabled(true);
	    }
    }
}
