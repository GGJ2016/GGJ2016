package ggj2016.view;

import ggj2016.model.AssetsManager;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
* View displayed when the player win or lose
* @author Les Adeptes de Mordak
*/
public class StartGamePanel extends EndGamePanel
{
    private final JButton buttonCredits;
    private final JButton buttonPlay;
    private final JButton buttonQuit;
    
    public StartGamePanel(String bgName, String msg)
    {
        super(bgName, msg);
        this.setLayout(null);
        BufferedImage imgQuit = AssetsManager.get().getImage("buttonquit");
        this.buttonQuit = new JButton(new ImageIcon(imgQuit));
        this.buttonQuit.setContentAreaFilled(false);
        this.buttonQuit.setBorderPainted(false);
        this.buttonQuit.setRolloverEnabled(false);
        this.buttonQuit.setFocusPainted(false);
        this.buttonQuit.setBounds(850, 350, imgQuit.getWidth(), imgQuit.getHeight());
        
        BufferedImage imgCredits = AssetsManager.get().getImage("buttoncredits");
        this.buttonCredits = new JButton(new ImageIcon(imgCredits));
        this.buttonCredits.setContentAreaFilled(false);
        this.buttonCredits.setBorderPainted(false);
        this.buttonCredits.setRolloverEnabled(false);
        this.buttonCredits.setFocusPainted(false);
        this.buttonCredits.setBounds(10, 350, imgCredits.getWidth(), imgCredits.getHeight());
        
        BufferedImage imgPlay = AssetsManager.get().getImage("buttonplay");
        this.buttonPlay = new JButton(new ImageIcon(imgPlay));
        this.buttonPlay.setContentAreaFilled(false);
        this.buttonPlay.setBorderPainted(false);
        this.buttonPlay.setRolloverEnabled(false);
        this.buttonPlay.setFocusPainted(false);
        this.buttonPlay.setBounds(100, 550, imgPlay.getWidth(), imgPlay.getHeight());
        
        this.add(this.buttonQuit);
        this.add(this.buttonCredits);
        this.add(this.buttonPlay);
    }
    
    /**
    * Provides the button Credits
    * @return      attribute buttonCredits
    */
    public JButton GetButtonCredits()
    {
        return this.buttonCredits;
    }
    
    /**
    * Provides the button Play
    * @return     attribute buttonPlay
    */
    public JButton GetButtonPlay()
    {
        return this.buttonPlay;
    }
    
    /**
    * Provides the button Quit
    * @return      attribute buttonQuit
    */
    public JButton GetButtonQuit()
    {
        return this.buttonQuit;
    }
}
