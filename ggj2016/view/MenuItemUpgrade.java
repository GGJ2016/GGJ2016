package ggj2016.view;

import javax.swing.JMenuItem;

public class MenuItemUpgrade extends JMenuItem
{
	ggj2016.model.Building buildingModel;
	
	public MenuItemUpgrade(String title, ggj2016.model.Building model)
	{
		super(title);
        
		this.buildingModel = model;
	}
	
	public ggj2016.model.Building getBuildingModel()
	{
		return this.buildingModel;
	}
}
