package ggj2016.view;

import ggj2016.model.Log;
import ggj2016.model.Model;
import ggj2016.model.AssetsManager;
import ggj2016.model.Game;

import java.awt.event.*;
import java.util.ArrayList;
import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

/**
 * Class for managing the display
 * @author Les Adeptes de Mordak
 */
public class View extends JFrame
{
    public boolean isGameRunning;
    public static final int VIEW_WIDTH = 1280;
    public static final int VIEW_HEIGHT = 720;
    private static final long serialVersionUID = 1L;
    private boolean finished;
    private CreditsGamePanel creditsPanel;
    private GamePanel gamePanel;
    private EndGamePanel losePanel;
    private JPanel mainPanel;
    private JMenuBar menuBar;
    private JMenu menuFile;
    private JMenuItem menuItemExit;
    private JMenuItem menuItemSummon;
    private ArrayList<MenuItemUpgrade> menuItemUpgrades;
    private JMenu menuMordak;
    private JMenu menuUpgrades;
    private StartGamePanel startPanel;
    private ResourcePanel resourcePanel;
    private EndGamePanel winPanel;
    private final BufferedImage logo;
    private final BufferedImage cursorImage;
    private final Model model;

    /**
    * Constructor
    * @param model      Model of the game
    */
    public View(Model model)
    {
        this.isGameRunning = false;
        this.model = model;
        this.initMenuBar();
        this.initPanels();
        this.finished = false;
        this.setTitle("The Call of Mordak");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(View.VIEW_WIDTH, View.VIEW_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        // Init the logo of the game
        this.logo = AssetsManager.get().getImage("logo");
        this.setIconImage(logo);
        // Init the custom cursor of the game
        this.cursorImage = AssetsManager.get().getImage("cursor");
        Point hotspot = new Point(0,0);
        String cursorName = "Mordak cursor";
        this.setCursor(getToolkit().createCustomCursor(cursorImage, hotspot, cursorName)); 
        this.setFocusable(true);
        this.setVisible(true);
    }
    
    public void addBackListener(ActionListener listener)
    {
        this.creditsPanel.GetButtonBack().addActionListener(listener);
    }
    
    public void addCreditsListener(ActionListener listener)
    {
        this.startPanel.GetButtonCredits().addActionListener(listener);
    }
    
   /**
    * Add a listener for the exit item
    * @param listener listener to add to the exit item
    */
    public void addExitListener(ActionListener listener)
    {
        this.menuItemExit.addActionListener(listener);
    }
    
    public void addPlayListener(ActionListener listener)
    {
        this.startPanel.GetButtonPlay().addActionListener(listener);
    }
    
    public void addQuitListener(ActionListener listener)
    {
       this.startPanel.GetButtonQuit().addActionListener(listener);
    }
    
    public void addSummonListener(ActionListener listener)
    {
        this.menuItemSummon.addActionListener(listener);
    }
    
    /**
    * Add a listener for the upgrades menu items
    * @param listener
    */
    public void addUpgradeListener(ActionListener listener)
    {
        for(MenuItemUpgrade item : menuItemUpgrades)
	    {
            item.addActionListener(listener);
	    }
    }
    
    /**
    * Method used to close the view
    */
    public void close()
    {
        this.setVisible(false);
        this.dispose();
    }
    
    /**
    *  Display the dialog box associated to an event
    * @param event     event to display
    */
    public void displayEvent(ggj2016.model.Event event)
    {
        try
	    {
            Object[] options = event.getChoices();
            event.executeEffects();
            int choice = JOptionPane.showOptionDialog(this, event.getMessage(), event.getTitle(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, new ImageIcon(AssetsManager.get().getImage("miniature")), options, options[0]);	
            
            if(event.hasReaction(choice))
		    {
                // Play sound of Mordak thinking 
                this.displayEvent(event.getReaction(choice));
		    }
	    }
        catch(Exception e)
	    {
            this.displayEvent(event);
	    }
        // Restore the music
        AssetsManager.get().getMusic().play();
    }
    
    /**
    * Return the panel where the game takes place
    * 
    * @return resourcePanel
    */
    public GamePanel getGamePanel()
    {
        return this.gamePanel;
    }
    
    /**
    * Return the panel where the resources are printed
    * @return resourcePanel
    */
    public ResourcePanel getResourcePanel()
    {
        return this.resourcePanel;
    }
    
    /**
    * Method to know if the view has been closed or not
    * @return value of the finished attribute
    */
    public boolean hasClosed()
    {
        return this.finished;
    }

    /**
    * Method used to set the attributed finished to true
    */
    public void stop()
    {
        this.finished = true;
    }
    
    public void switchToCredits()
    {
        this.isGameRunning = false;
        this.menuBar.setVisible(false);
        this.setContentPane(this.creditsPanel);
        this.validate();
    }
    
    /**
    * Display the game screen
    */
    public void switchToGame()
    {
        this.isGameRunning = true;
        this.menuBar.setVisible(true);
    	this.setContentPane(this.mainPanel);
    	validate();

        AssetsManager.get().getMusic().stop();
        AssetsManager.get().playMusic("game"); // throws the music
    }
    
    /**
    * Display the losing screen
    */
    public void switchToLoseGame()
    {
        this.isGameRunning = false;
        // end game
        this.menuBar.setVisible(false);
        this.setContentPane(this.losePanel);

        AssetsManager.get().getMusic().stop();
        AssetsManager.get().playMusic("lose"); // throws the music

        this.validate();
    }

    /**
    * Display the start screen
    */
    public void switchToStart()
    {
        this.isGameRunning = false;
        // start panel
        this.menuBar.setVisible(false);
        this.setContentPane(this.startPanel);
        this.validate();
    }
    
    /**
    * Display the winning screen
    */
    public void switchToWinGame()
    {
        this.isGameRunning = false;
        this.menuBar.setVisible(false);
        this.setContentPane(this.winPanel);
        this.validate();
    }
    
    @SuppressWarnings("deprecation") // eclipse, il m'a dit je peux - Jeff
    public void updateMenuUpgrades()
    {
        Game g = model.getGame();
        int sp = (int) g.getSummonPoints().getAmount();
        
        for(MenuItemUpgrade item : menuItemUpgrades)
	    {
            ggj2016.model.Building b = item.getBuildingModel();
            int costGold = b.getCostGold();
            int costMaterials = b.getCostMaterials();
            int costSP = b.getCostSP();
            String label = "";
            String name = b.getName();
            int level = b.getLevel();
            
            if(level == 0)
            {
                label += "Buy " + name;
            }
            else
            {
                label += "Upgrade " + name + " "+ level++ + " -> " + level;
            }
            int lenght = label.length();
            
            while(lenght++ < 20)
            {
                label += " ";
            }
            label += " < [Gold : " + costGold + "] ";
            label += "[Materials : " + costMaterials + "] ";
            label += "[Summon Points : " + costSP + "] >";
            item.setLabel(label);
            int gold = (int) g.getGold().getAmount();
            int materials = (int) g.getMaterials().getAmount();
            if((gold >= costGold) && (materials >= costMaterials) && (sp >= costSP))
            {
                this.enableMenuItem(item);
            }
            else
            {
                this.disableMenuItem(item);
            }
	    }	
        // summon mordak
        if(sp >= 7500)
        {
            this.enableMenuItem(menuItemSummon);
        }
        else
        {
            this.disableMenuItem(menuItemSummon);
        }
    }
    
    private void disableMenuItem(JMenuItem item)
    {
        if(item.isEnabled())
	    {
            item.setEnabled(false);
	    }
    }

    private void enableMenuItem(JMenuItem item)
    {
        if(!item.isEnabled())
	    {
            item.setEnabled(true);
	    }
    }
    
    /**
     * Initialize the menu bar
     */
    private void initMenuBar()
    {
        // Menu Bar
        this.menuBar = new JMenuBar();	
        // Menu File
        this.menuFile = new JMenu("File");
        this.menuFile.setMnemonic(KeyEvent.VK_F);
        this.menuItemExit = new JMenuItem("Exit");
        this.menuFile.add(menuItemExit);
        this.menuBar.add(menuFile);	
        // Menu upgrades
        this.menuUpgrades = new JMenu("Upgrades");
        this.menuUpgrades.setMnemonic(KeyEvent.VK_U);
        this.menuItemUpgrades = new ArrayList<>();
        this.initMenuUpgrades();
        this.menuBar.add(menuUpgrades);
        this.menuMordak = new JMenu("Mordak");
        this.menuMordak.setMnemonic(KeyEvent.VK_M);
        this.menuItemSummon = new JMenuItem("Summon Mordak [7500 Summon Points]");
        this.menuMordak.add(menuItemSummon); 
        this.menuItemSummon.setEnabled(false);
        this.menuBar.add(menuMordak);
        this.setJMenuBar(menuBar);
        this.menuBar.setVisible(false);
    }
	
    private void initMenuUpgrades()
    {
        ggj2016.model.Building[] b = model.getGame().getBuildings();
        int lenght = b.length;
        for (int i = 0 ; i < lenght ; i++)
	    {
            MenuItemUpgrade item = new MenuItemUpgrade(b[i].getName(), b[i]);
            this.menuItemUpgrades.add(item);
            this.menuUpgrades.add(item);
	    }
    }
    
    /**
    * Initialize the panels
    */
    private void initPanels()
    {
        {
            // main panel
            this.mainPanel = new JPanel();
            this.mainPanel.setLayout(new BorderLayout());
            this.resourcePanel = new ResourcePanel();
            this.gamePanel = new GamePanel(this.model);
            this.mainPanel.add(this.resourcePanel, BorderLayout.NORTH);
            this.mainPanel.add(this.gamePanel, BorderLayout.CENTER);
        }
        // end game
        {
            this.winPanel = new EndGamePanel("won", "test"); // DEBUG
            this.losePanel = new EndGamePanel("lose-txt", "test"); // DEBUG
        }
        // menu game
        {
            this.startPanel = new StartGamePanel("opening", "test"); // DEBUG
        }
        // credits
        {
            this.creditsPanel = new CreditsGamePanel("credits", "test"); // DEBUG
        }
        this.switchToGame();
    }
}


