package ggj2016.view;

import ggj2016.model.AssetsManager;
import ggj2016.model.Log;
import ggj2016.model.Model;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Main panel for the game 
 * @author Les Adeptes de Mordak
 */
public class GamePanel extends JPanel
{
    private final BufferedImage background;
    private final ArrayList<ggj2016.view.Building> buildings;
    private final Model model;

    public GamePanel(Model model)
    {
        super();
        
        this.setLayout(null);
        this.model = model;
        this.buildings = new ArrayList<>();
        this.background = AssetsManager.get().getImage("background");
        
        if(this.model == null)
	    {
            Log.write("model is null");
	    }
    }
    
    /**
    *  Add all the buildings of the model into the game panel
    */
    public void addAllBuildings()
    {
        for(int i = 0 ; i < this.model.getGame().getBuildings().length ; i++)
	    {
            this.addBuilding(this.model.getGame().getBuildings()[i]);
	    }
    }

    public ArrayList<ggj2016.view.Building> getBuildings()
    {
        return this.buildings;
    }
    
    /**
     * Add a building in the game panel
     * @param building to add to the game panel
     */
    public void addBuilding(ggj2016.model.Building building)
    {		
        BufferedImage bg = AssetsManager.get().getImage("background");
        double bgW = bg.getWidth();
        double bgH = bg.getHeight();

        double ratioW = getWidth()/bgW;
        double ratioH = getHeight()/bgH;

        BufferedImage img = AssetsManager.get().getImage(building.getName());
        double w = img.getWidth() * ratioW;
        double h = img.getHeight() * ratioH;

        BufferedImage newImage;
        {
            newImage = new BufferedImage((int)w, (int)h, BufferedImage.TYPE_INT_ARGB);

            Graphics g = newImage.createGraphics();
            g.drawImage(img, 0, 0, (int)w, (int)h, null);
            g.dispose();
        }

        Building viewBuilding = new Building(newImage, building);
        viewBuilding.setBounds((int) building.getPosition().getX(), (int) building.getPosition().getY(), (int)w +55, (int)h +55);
	
        this.add(viewBuilding);
        this.buildings.add(viewBuilding);
    }

    /**
     * Draw the background
     * @param g Component to draw
     */
    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage(this.background, 0, 0, getWidth(), getHeight(), this);
	
        if(this.background == null)
	    {
            Log.write("Impossible d'afficher le background !");
            g.setColor(new Color(4, 139, 154));
            g.fillRect(0, 0, getWidth(), getHeight());
	    }
    }




}
