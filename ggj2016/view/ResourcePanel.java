package ggj2016.view;

import ggj2016.model.AssetsManager;
import ggj2016.model.Log;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;


/**
*  Panel for the player resources
*  @author Les Adeptes de Mordak
*/
public class ResourcePanel extends JPanel
{
    private final BufferedImage resourcesBar;
    private ImageIcon populationIcon;
    private JLabel populationLabel;
    private ImageIcon materialsIcon;
    private JLabel materialsLabel;
    private ImageIcon goldIcon;
    private JLabel goldLabel;
    private ImageIcon summonIcon;
    private JLabel summonLabel;
    private ImageIcon trustIcon;
    private JLabel trustLabel;
    
    public ResourcePanel()
    {
        super();
        
        this.resourcesBar = AssetsManager.get().getImage("resourcesbar");
        
        this.initLabel();
    }

    /**
    *  Initialize resources labels
    */
    public void initLabel()
    {
        this.trustIcon = new ImageIcon(AssetsManager.get().getImage("trust"));
        this.trustLabel = new JLabel(" : 0", this.trustIcon, JLabel.CENTER);
        this.trustLabel.setForeground(Color.WHITE);

        this.populationIcon = new ImageIcon(AssetsManager.get().getImage("population"));
        this.populationLabel = new JLabel(" : 0 / 30", this.populationIcon, JLabel.CENTER);
        this.populationLabel.setForeground(Color.WHITE);

        this.goldIcon = new ImageIcon(AssetsManager.get().getImage("goldgem"));
        this.goldLabel = new JLabel(" : 0", this.goldIcon, JLabel.CENTER);
        this.goldLabel.setForeground(Color.WHITE);

        this.materialsIcon = new ImageIcon(AssetsManager.get().getImage("materials"));
        this.materialsLabel = new JLabel(" : 0", this.materialsIcon, JLabel.CENTER);
        this.materialsLabel.setForeground(Color.WHITE);

        this.summonIcon = new ImageIcon(AssetsManager.get().getImage("summon"));
        this.summonLabel = new JLabel(" : 0", this.summonIcon, JLabel.CENTER);
        this.summonLabel.setForeground(Color.WHITE);

        this.setLayout(new GridLayout(1, 5));
        this.add(trustLabel, 0, 0);
        this.add(populationLabel, 0, 1);
        this.add(goldLabel, 0, 2);
        this.add(materialsLabel, 0, 3);
        this.add(summonLabel, 0, 4);
    }
    
    /**
    *  Draw the background
    * @param g     Component to paint
    */
    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage(this.resourcesBar, 0, 0, getWidth(), getHeight(), this);
        if(this.resourcesBar == null)
	    {
            Log.write("Impossible d'afficher la barre de ressources !");
            g.setColor(new Color(4, 139, 154));
            g.fillRect(0, 0, getWidth(), getHeight());
	    }
    }

    /**
    *  Define the gold amount
    *  @param gold the amount of gold
    */
    public void setGold(double gold)
    {
        this.goldLabel.setText(" : " + (int)gold);
    }

    /**
    *  Define the materials amount
    *  @param materials the amount of materials
    */
    public void setMaterials(double materials)
    {
        this.materialsLabel.setText(" : " + (int)materials);
    }
    
    /**
    *  Define the number of inhabitants
    *  @param available population available 
    *  @param total the total of population (available + workers)
    *  @param max maximum of inhabitant allowed
    */
    public void setPopulation(double available, double total, double max)
    {
        this.populationLabel.setText(" : " + (int)available + " / " + (int)total + " (max " + (int)max + ")");
    }
    
    /**
    *  Define the SummonPoints amount
    *  @param summon the amount of SummonPoints
    */
    public void setSummonPoints(double summon)
    {
        this.summonLabel.setText(" : " + (int)summon);
    }

    /**
    *  Define the TrustPoints amount
    *  @param trust the amount of TrustPoints
    */
    public void setTrust(double trust)
    {
        this.trustLabel.setText(" : " + (int)trust);
    }
}
