package ggj2016.view;

import ggj2016.model.AssetsManager;
import ggj2016.model.Log;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

/**
 * View displayed when the player win or lose
 * @author Les Adeptes de Mordak
 */
public class EndGamePanel extends JPanel
{
    private final BufferedImage background;
    
    /**
    *  Constructor
    *  @param bgName    name of the background
    *  @param msg       message to write
    */
    public EndGamePanel(String bgName, String msg)
    {
		this.background = AssetsManager.get().getImage(bgName);
    }
    
   /**
	* Draw the background
	* @param g  Component to draw
	*/
	@Override
	public void paintComponent(Graphics g)
	{
		g.drawImage(this.background, 0, 0, this.getWidth(), this.getHeight(), this);
		if(this.background == null)
		{
			Log.write("Impossible d'afficher le background de fin du jeu !");
			g.setColor(new Color(4, 139, 154));
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

}
