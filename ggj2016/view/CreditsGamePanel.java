package ggj2016.view;

import ggj2016.model.AssetsManager;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *  Credits of the game
 *  @author Les Adeptes de Mordak
 */
class CreditsGamePanel extends EndGamePanel
{
    private final JButton buttonBack;
    
    public CreditsGamePanel(String bgName, String msg)
    {
        super(bgName, msg);
        
        BufferedImage imgBack = AssetsManager.get().getImage("buttonback");
        this.buttonBack = new JButton(new ImageIcon(imgBack));
        this.buttonBack.setContentAreaFilled(false);
        this.buttonBack.setBorderPainted(false);
        this.buttonBack.setRolloverEnabled(false);
        this.buttonBack.setFocusPainted(false);
        this.buttonBack.setBounds(300, 10, imgBack.getWidth(), imgBack.getHeight());
        
        this.add(this.buttonBack);
    }
    
    /**
    * Provides the button Back
    * @return      attribute buttonBack
    */
    public JButton GetButtonBack()
    {
        return this.buttonBack;
    }
}
