package ggj2016.model;

/**
   Effect executed by an event
*/
public class ResourceEffect extends Effect
{
    private double gold;
    private double materials;
    private double population;
    private double summonPoints;
    private double trust;
    
    /**
    *  Constructor
    *  @param game affected by this effect
    */
    public ResourceEffect(Game game)
    {
        this(game, 0, 0, 0, 0, 0);
    }

    public ResourceEffect(Game game, double trust, double population, double gold, double materials, double summonPoints)
    {
        super(game);
        
        this.setTrust(trust);
        this.setPopulation(population);
        this.setGold(gold);
        this.setMaterials(materials);
        this.setSummonPoints(summonPoints);
    }
    
    @Override
    public void execute()
    {
        this.game.getGold().add(this.gold);
        this.game.getTrust().add(this.trust);
        House house = (House)(this.game.getBuilding("House"));
        this.game.getMaterials().add(this.materials);
        this.game.getSummonPoints().add(this.summonPoints);

	    if(this.population < 0) // Si on veut retirer des citoyens plutôt qu'en ajouter
        {
            double leftToRemove = Math.abs(this.population);
            if(this.game.getPopulation().isWorkerAvailable(leftToRemove))
            {
                this.game.getPopulation().add(this.population, (int)house.getSpace());
            }
            else
            {
                Building [] buildings = this.game.getBuildings();
                for(Building b : buildings) // Pour chaque batiment, on essaie de retirer autant de citoyens que possible jusqu'à ce qu'il n'y ai plus besoin d'en retirer
                {
                    if(b.getCurrentWorkers() < leftToRemove)
                    {
                        double newLeftToRemove = leftToRemove - b.getCurrentWorkers();
                        double removed = Math.abs(b.getCurrentWorkers()-leftToRemove);
                        b.removeWorkers((int)removed);
                        leftToRemove = newLeftToRemove;
                    }
                    else
                    {
                        b.removeWorkers((int)leftToRemove);
                        leftToRemove = 0;
                    }
                }
                if(leftToRemove > 0)
                {
                    Log.write("Problem when removing population: there is a need to remove people");
                }
            }
        }
            this.game.getPopulation().add(this.population, (int)house.getSpace());	    
    }

    /**
    *  Change the gold modification implies by the effect
    *  @param gold added (or removed) to the play resources
    */
    public void setGold(double gold)
    {
        this.gold = gold;
    }    

    /**
    *  Change the materials modification implies by the effect
    *  @param materials added (or removed) to the play resources
    */
    public void setMaterials(double materials)
    {
        this.materials = materials;
    }

    /**
    *  Change the population modification implies by the effect
    *  @param population added (or removed) to the play resources
    */
    public void setPopulation(double population)
    {
        this.population = population;
    }    
    
    /**
    *  Change the SummonPoints modification implies by the effect
    *  @param summonPoints added (or removed) to the play resources
    */
    public void setSummonPoints(double summonPoints)
    {
        this.summonPoints = summonPoints;
    }
    
    /**
    *  Change the trust modification implies by the effect
    *  @param trust added (or removed) to the play resources
    */
    public void setTrust(double trust)
    {
        this.trust = trust;
    }
}
