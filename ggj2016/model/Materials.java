package ggj2016.model;

public class Materials extends Resource
{
    public Materials()
    {
      this.amount = 0;
    }
  
    public void spend(int amount)
    {
      if(hasEnough(amount))
      {
        this.amount -= amount;
      }
      else
      {
        Log.write("Not enough materials"); // TODO meilleur message d'erreur
      }
    }
}
