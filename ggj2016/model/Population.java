package ggj2016.model;

public class Population
{
    private double amountAvailable;
    private double amountTotal;
    private double birthRate;
    private double nbNewInhabitants;

    public Population()
    {
        this.amountTotal = 0;
        this.amountAvailable = amountTotal;
        this.birthRate = 1.05;
        this.nbNewInhabitants = 0;
    }

    public void add(double amount)
    {
        this.amountTotal += amount;
        this.amountAvailable += amount;
    }
    
    public void add(double amount, int max)
    {
        Log.write("Ajout de " + amount + " (max = " + max +")");
	
        if((this.amountAvailable + amount) > max)
	    {
            double res = (double)max - this.amountAvailable;
	        this.add(res);
	    }
        else
	    {
            this.add(amount);
	    }
    }
    
    public void fireWorker(double amount)
    {
        if((this.amountAvailable + amount) <= this.amountTotal)
	    {
            this.amountAvailable += amount;
	    }
        else
	    {
            Log.write("Max population exceeded"); // TODO meilleur message d'erreur
	    }
    }

    public boolean isWorkerAvailable(double amount)
    {
        return (this.amountAvailable >= amount);
    }

    public double getAvailable()
    {
        return this.amountAvailable;
    }

    public double getBirthRate()
    {
        return this.birthRate;
    }    
    
    public double getTotal()
    {
        return this.amountTotal;
    } 
    
    public void hireWorker(double amount)
    {
        if (isWorkerAvailable(amount))
	    {
            this.amountAvailable -= amount;
	    }
        else
	    {
            Log.write("Not enough population available"); // TODO meilleur message d'erreur
	    }
    } 

    public void increasePopulation(double max)
    {
        if(this.amountTotal<max)
        {
            this.nbNewInhabitants += this.amountAvailable * (this.birthRate - 1.0);
            while(nbNewInhabitants>=1)
            {
                nbNewInhabitants--;
                this.add(1.0);
            }
        }
    }    
    
    public void modifBirthRate(double modifRate)
    {    
        if((this.birthRate + modifRate) >= 1.0)
        {
            this.birthRate += modifRate;
        }
        else
        {
            this.birthRate = 1.0;
            Log.write("A birthrate minor than 1 is not allowed");  // TODO meilleur message d'erreur
        }
    }    
    
    public void reduce(double amount)
    {
        if(isWorkerAvailable(amount))
	    {
            this.amountTotal -= amount;
            this.amountAvailable -= amount;
	    }
    }
    
    public void setBirthRate(double br)
    {
        if(br >= 1.0)
        {
            this.birthRate = br;
        }
        else
        {
            this.birthRate = 1.0;
        }
    }
}
