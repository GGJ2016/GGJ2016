package ggj2016.model;

public class Workshop extends BuildingResource
{
    public Workshop(Population pop)
    {
        super(pop);
        
        this.level = 0;
        this.name = "Workshop";
        this.position = new Position(80.0, 275.0);
        this.currentWorkers = 0;
        this.maxWorkers = 0;
        this.currentIncome = 0.;
        this.baseIncome = 0.7;

        this.setBaseCosts(500, 400, 0);		
    }
}
