package ggj2016.model;

public class Game
{
    public final static int BUILDINGMAX = 4; // TODO increments if we add more building to the game
    private boolean mordakSummoned;
    private final Gold gold;
    private final Materials materials;
    private final Population population;
    private final SummonPoints sp;
    private final Trust trust;
    private Building[] buildings;

    public Game()
    {
        this.mordakSummoned = false;

        Log.write("New game !");

        this.population = new Population();
        this.population.add(10);

        this.materials = new Materials();
        this.materials.add(500);

        this.gold = new Gold();
        this.gold.add(500);

        this.sp = new SummonPoints();
        this.sp.add(500);

        this.trust = new Trust();
        this.trust.add(50);

        initBuildings();
    }
    
    public Building[] getBuildings()
    {
        return this.buildings;
    }

    public Building getBuilding(String name)
    {
        int index=0;
      
        while(index < this.buildings.length)
	    {
            if(this.buildings[index].getName().equals(name))
		    {
                return this.buildings[index];
		    }
            index++;
	    }
      
        return null;
    }
    
    public Gold getGold()
    {
        return this.gold;
    }
    
    public Materials getMaterials()
    {
        return this.materials;
    }

    public Population getPopulation()
    {
        return this.population;
    }
    
    public SummonPoints getSummonPoints()
    {
        return this.sp;
    }
    
    public Trust getTrust()
    {
        return this.trust;
    }

    public void payGold(int amount)
    {
        this.gold.spend(amount);
    }
    
    public void payMaterials(int amount)
    {
        this.materials.spend(amount);
    }
    
    public void paySP(int amount)
    {
        this.sp.spend(amount);
    }
    
    /**
    *  Check if the player loses the game
     * @param minute    actual minute of the game
    *  @return          true if the player lost the game
    */
    public boolean playerLoses(int minute)
    {
        return ((this.sp.getAmount() < 1) || (this.trust.getAmount() < 1) || (minute >= 15)); //TODO  not enough resources to pay for event
    }
    
    /**
    * Check if the player wins the game
    * @return  Return true if the player has won the game
    */
    public boolean playerWins()
    {
        return ((this.trust.getAmount() > 99) || (this.mordakSummoned));
    }
    
    public void summonMordak()
    {
        this.mordakSummoned = true;
    }
    
    private void initBuildings()
    {
        this.buildings = new Building[BUILDINGMAX];
        this.buildings[0] = new Mine(this.population);    
        this.buildings[1] = new Graveyard(this.population);
        this.buildings[2] = new House();
        this.buildings[3] = new Workshop(this.population);
    }
}
