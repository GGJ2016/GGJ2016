package ggj2016.model;

/*
*  Effect executed by an event
*/
public abstract class Effect
{
    protected Game game;
    
    public abstract void execute();
    
    public Effect(Game game)
    {
        this.game = game;
    }
}
