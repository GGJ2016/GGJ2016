package ggj2016.model;

import java.util.*;
import java.awt.image.*;
import java.io.*;
import java.net.URL;
import java.applet.Applet;
import java.applet.AudioClip;

import javax.imageio.*;

/**
*  Manage the assets of the game and give access to them in the whole game.
*   It implements singleton design pattern.
*   @author bog
*/
public class AssetsManager
{
    private static AssetsManager instance;
    private AudioClip music;
    private String musicName; // to avoid repetitive call
    private final String audioDir; // Spécification obligée dûe à l'utilisation de URL()
    private final String dataDir;
    private final HashMap<String, BufferedImage> images;
    private final HashMap<String, String> musicPaths; // when we ask for a music, we give an ID which matches with a path
    private final HashMap<String, AudioClip> sounds;
    
    private AssetsManager()
    {
        this.images = new HashMap<>();
        this.sounds = new HashMap<>();	
        this.musicPaths = new HashMap<>();
        this.music = null;
        this.musicName = new String();
        this.dataDir = "/assets";
        this.audioDir= "assets/";
        
        load();
    }
    
    /**
    *  @return the unique instance of AssetsManager
    */
    public static AssetsManager get()
    {
        if(AssetsManager.instance == null)
	    {
            AssetsManager.instance = new AssetsManager();
	    }
        return AssetsManager.instance;
    }   

    /**
    *  Allow to get a buffered image using its name
    *  @param name of the image to return
    *  @return the buffered image associated with the name or null if the image does not exists in the assets manager
    */
    public BufferedImage getImage(String name)
    {
        BufferedImage image = this.images.get(name);
        return image;
    }

    /*
    * return the current music
    */
    public AudioClip getMusic()
    {
        return this.music;
    }
    
    /*
    * return the current music
    */
    public AudioClip getMusic(String name)
    {	
        try
	    {
            String path = this.musicPaths.get(name);
            if(path == null)
		    {
                System.out.println("Music not found : " + name);
		    }
				
            URL file = getClass().getClassLoader().getResource(path);
	        this.music = Applet.newAudioClip(file);      
	    }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return this.music;
    }
    
    /** 
    *  return a sound stored
    *  @param name the name of the sound stored
    *  @return the sound corresponding
    */
    public AudioClip getSound(String name)
    {
        AudioClip audio = this.sounds.get(name);
        if(audio == null)
	    {
            System.out.println("Sound not found ! ");
	    }
        return audio;
    }
    
    /**
    * @param name that reference the sound
    * @param path where to load the sound
    */
    public void loadMusic(String name, String path)
    {
        String complete_path = this.audioDir + path;
        this.musicPaths.put(name, complete_path);
	
        try
	    {
            URL file = getClass().getClassLoader().getResource(complete_path);
	        this.music = Applet.newAudioClip(file);
	    }
        catch(Exception e)
        {
            Log.write("Problem at load music");
            e.printStackTrace();
        }
    }
    
    /**
    * @param name  that reference the sound
    * @param path  where to load the sound
    * @return      the sound
    */
    public AudioClip loadSound(String name, String path)
    {
        String complete_path = this.audioDir + path;
        AudioClip audio = null;
	
        try
	    {
            URL file = getClass().getClassLoader().getResource(complete_path);
	        audio = Applet.newAudioClip(file);
            this.sounds.put(name, audio);
	    }
        catch(Exception e)
        { 
            Log.write("Problem at load sound");
            e.printStackTrace();
        }

        return audio;
    }

    /*
    * allows to loop the music
    * @param name   music to loop
    */
    public void playMusic(String name)
    {
        if(!this.musicName.equals(name))
	    {
            this.musicName = name;
            this.music.stop();
            getMusic(name).loop();
	    }
    }
    
    /**
    *  Load an image and push it in the assets manager
    *  @param name that reference the image
    *  @param path where to load the image
    *  @throws Exception if the image can't be loaded
    */
    private void loadImage(String name, String path) throws Exception
    {
        String complete_path = this.dataDir + "/" + path;
        InputStream is = getClass().getResourceAsStream(complete_path);
        BufferedImage image = ImageIO.read(is);
	
        if(image == null)
	    {
            throw new NullPointerException();
	    }
	
        this.images.put(name, image);
    }
    
    /**
    *  Load all the assets
    */
    private void load()
    {
        try
	    {  
            /* LIST OF ASSETS TO LOAD */
            loadImage("default", "images/default.jpg");
            /* MINIATURE AND LOGO */
            loadImage("miniature", "images/miniature.png");
            loadImage("logo", "images/logo.png");
            /* CURSOR */
            loadImage("cursor", "images/cursor.png");
            /* MAINS SCREENS */
            loadImage("background", "images/backgrounds/background.png");
            loadImage("opening", "images/backgrounds/opening.png");
            loadImage("won", "images/backgrounds/won.png");
            loadImage("lose-txt", "images/backgrounds/lose-txt.png");
            loadImage("credits", "images/backgrounds/credits.png");
            /* ICONS AND RESOURCE BAR*/
            loadImage("resourcesbar", "images/resourcesbar.png");
            loadImage("population", "images/icons/townfolk.png");
            loadImage("goldgem", "images/icons/goldgem.png");
            loadImage("materials", "images/icons/materials.png");
            loadImage("summon", "images/icons/summon.png");
            loadImage("trust", "images/icons/trust.png");
            /* BUTTONS */
            loadImage("buttonadd", "images/buttons/addBtn.png");
            loadImage("buttonremove", "images/buttons/removeBtn.png");
            loadImage("buttonquit", "images/buttons/quitBtn.png");
            loadImage("buttoncredits", "images/buttons/creditBtn.png");
            loadImage("buttonplay", "images/buttons/startBtn.png");
            loadImage("buttonback", "images/buttons/backBtn.png");
            /* BUILDINGS */
            loadImage("Graveyard", "images/buildings/graveyard.png");
            loadImage("Mine", "images/buildings/mine.png");
            loadImage("House", "images/buildings/town.png");
            loadImage("Workshop", "images/buildings/workshop.png");
            /* SOUNDS */
            loadSound("default", "sounds/default.wav");
            loadSound("upgrade", "sounds/build.wav");
            loadSound("click+", "sounds/click+.wav");
            loadSound("click-", "sounds/click-.wav");
            loadSound("up", "sounds/lvl_up.wav");
            loadSound("roar", "sounds/roar.wav");
            /* MUSICS */
            loadMusic("opening", "musics/opening.wav");
            loadMusic("game", "musics/game.wav");
            loadMusic("lose", "musics/lose.wav");
            loadMusic("victory", "musics/victory.wav");
	    }
        catch(Exception e)
	    {
            e.printStackTrace();
	    }
    } 
}

