package ggj2016.model;

public abstract class Resource
{
    protected double amount;

    public void add(double amount)
    {
        this.amount += amount;
	
        if( this.amount < 0 )
	    {
            this.amount = 0;
	    }
    }
    
    public double getAmount()
    {
        return this.amount;
    }
    
    public boolean hasEnough(int amount)
    {
        return (amount <= this.amount);
    }
}
