package ggj2016.model;

public abstract class Building
{
	protected int baseCostGold;
	protected int baseCostMaterials;
	protected int baseCostSP;
    protected int currentWorkers;
    protected int level;
	protected int maxWorkers;
    protected String name;
	protected Position position;
	
    public abstract boolean addWorkers(int amount);
    public abstract void editLevel(int nbLevel);
    public abstract void destroyBuilding();
	public abstract boolean removeWorkers(int amount);
    
    public int getCostGold()
	{
	    if(this.level == 0)
	    {
	      return 70;  	  	
	    }
	      	  	
	    return (this.baseCostGold * this.level * this.level);
	}
    
    public int getCostMaterials()
	{
	    if(this.level == 0)
	    {
	      return 70;  	  	
	    }
	      	  	
	    return (this.baseCostMaterials * this.level * this.level);
	}

	public int getCostSP()
	{
	    if(this.level == 0)
	    {
	      return 0; 	  	
	    }
	      	  	
	    return (this.baseCostSP * this.level * this.level);
	}
    
    public int getCurrentWorkers()
	{
		return this.currentWorkers;
	}
    
    public int getLevel()
	{
		return this.level;
	}
    
    public int getMaxWorkers()
	{
		return this.maxWorkers;
	}
    
	public String getName()
	{
		return this.name;
	}

	public Position getPosition()
	{
		return this.position;
	}

    public void setBaseCosts(int gold, int materials, int sp)
	{
		this.baseCostGold = gold;
		this.baseCostMaterials = materials;
		this.baseCostSP = sp;
	}
    
	public void setPosition(double x, double y)
	{
		this.position.setX(x);
		this.position.setY(y);
	}
}
