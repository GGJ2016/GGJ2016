package ggj2016.model;

public class Log
{
    private static final boolean DEBUG = false;
    
    public static void write(String msg)
    {
        if(Log.DEBUG)
        {
            System.out.println(msg);
        }
    }
}
