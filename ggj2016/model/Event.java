package ggj2016.model;

import java.util.ArrayList;

/*
*  Event that make the player do a choose and affect its resources.
*
*  An event can trigger another event (= reaction)
*  An event has an effect on the game (= effect)
*
*  @see Effect
*/
public class Event
{
    protected boolean effectAlreadyExecuted;
    protected String[] choices;
    protected String message;
    protected String title;
    protected ArrayList<Effect> effects;
    protected ArrayList<Event> reactions;

    /*
    *  Constructor
    *  @param message   given by the event
    *  @param title     title of the event
    */
    public Event(String title, String message)
    {
        this(title, message, new String[]{"Okay"});
    }
    
    /*
    *  Constructor
    *  @param message given by the event
    *  @param choices the player can take
    */
    public Event(String title, String message, String[] choices)
    {
        this.choices = choices;
        this.effectAlreadyExecuted = false;
        this.message = message;
        this.title = title;
        this.effects = new ArrayList<>();
        this.reactions = new ArrayList<>();
    }

    /**
    *  @param effect to add to the event
    */
    public void addEffect(Effect effect)
    {
        this.effects.add(effect);
    }
    
    public void addReaction(Event reaction)
    {
        this.reactions.add(reaction);
    }

    /**
    *  execute all the effect of the event
    */
    public void executeEffects()
    {
        if( !this.effectAlreadyExecuted )
	    {
            for(Effect e : this.effects)
		    {
                e.execute();
		    }
            
            this.effectAlreadyExecuted = true;
	    }
    }
    
    /**
    *  @return the choices that the player can make
    */
    public String[] getChoices()
    {
        return this.choices;
    }
    
    /**
    *  @return the message displayed by the event
    */
    public String getMessage()
    {
        return this.message;
    }
    
    public Event getReaction(int index)
    {
        return this.reactions.get(index);
    }

    /**
    *  @return the title displayed by the event
    */
    public String getTitle()
    {
        return this.title;
    }
    
    public boolean hasReaction(int index)
    {
        return ((this.reactions.size()-1) >= index);
    }
}
