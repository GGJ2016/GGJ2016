package ggj2016.model;

/**
* Class for managing the running of the application
* @author Les Adeptes de Mordak
*/
public class Model
{  
    private final Game game;
  
    /**
    * Model's Contructor
    */
    public Model()
    { 
        this.game = new Game();
    }

    /**
     * Provides the game attribute of the model
     * @return  attribute game
     */
    public Game getGame()
    {
        return this.game;
    }
}
