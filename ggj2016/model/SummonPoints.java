package ggj2016.model;

public class SummonPoints extends Resource
{
    public SummonPoints()
    {
      this.amount = 0;
    }

    public void spend(int amount)
    {
      if(hasEnough(amount))
      {
        this.amount -= amount;
      }
      else
      {
        Log.write("Not enough summon points"); // TODO meilleur message d'erreur
      }
    }
}
