package ggj2016.model;

public abstract class BuildingResource extends Building
{
    protected double baseIncome;
    protected double currentIncome;
    protected Population population;
    
    public BuildingResource(Population population)
    {
        this.population = population;
    }
    
    @Override
    public boolean addWorkers(int amount)
    {
        if(((this.currentWorkers + amount) <= (this.maxWorkers))&& (this.population.isWorkerAvailable(amount)))
	    {
            this.currentWorkers += amount;
            this.population.hireWorker(amount);
            this.updateIncome();
            
            return true;
	    }
        
        Log.write("Max workers exceeded"); // TODO meilleur message d'erreur
        
        return false;
    }
    
    @Override
    public void destroyBuilding() // does not remove workers
    {
        this.level = 0;
        boolean removedWorkers = this.removeWorkers(this.currentWorkers);
        if(removedWorkers)
        {
            Log.write("Workers removed succesfully !");
        }
        else
        {
            Log.write("Problem when removing workers");
        }
        if (this.currentWorkers != 0)
	    {
            Log.write("This building still contains Workers !"); // TODO meilleur msg d'erreur
	    }
        this.updateIncome();
    }
    
    @Override
    public void editLevel(int nbLevel)
    {
        this.level += nbLevel;
        if(this.level < 0)
	    {
            this.level = 0;
	    }
        
        this.updateIncome();
        this.maxWorkers = this.level * 10 ; // TODO adjust ? (maybe, whatever, It's your life you know)
        if(this.currentWorkers>this.maxWorkers)
        {
            boolean removedWorkers = this.removeWorkers(this.currentWorkers-this.maxWorkers);
            if(removedWorkers)
            {
                Log.write("Workers removed succesfully !");
            }
            else
            {
                Log.write("Problem when removing workers");
            }
        }
        
    }
    
    public double getBaseIncome()
    {
        return this.baseIncome;
    }

    public double getCurrentIncome()
    {
        return this.currentIncome;
    }
    
    @Override
    public boolean removeWorkers(int amount)
    {
        if((this.currentWorkers - amount >= 0) && ((this.population.getAvailable() + amount) <= (this.population.getTotal())))
	    {
            this.currentWorkers -= amount;
            this.population.fireWorker(amount);
            this.updateIncome();
            
            return true;
	    }

        Log.write("No workers left to remove"); // TODO meilleur message d'erreur
        
        return false;
    }
    
    public void setBaseIncome(double newIncome)
    {
        this.baseIncome = newIncome;
        this.updateIncome();
    }

    protected void updateIncome()
    {
        this.currentIncome = this.baseIncome * this.level * this.currentWorkers;
    }
}
