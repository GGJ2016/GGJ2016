package ggj2016.model;

public class House extends Building
{
    private double space;

	public House()
	{
		this.level = 0;
		this.name = "House";
		this.position = new Position(470.0, 100.0);
		this.currentWorkers = 0;
		this.maxWorkers = 0;

		this.setBaseCosts(200, 200, 0); // TODO edit ?
		this.updateSpace();
	}

    @Override
	public boolean addWorkers(int amount)
	{
		return false;
	}
    
    @Override
	public void destroyBuilding()
	{
		this.level = 0;
		this.updateSpace();
	}
    
    @Override
	public void editLevel(int nbLevel)
	{
		this.level += nbLevel;
		if(this.level < 0)
		{
			this.level = 0;
		}
		this.updateSpace();
	}
    
    @Override
    public int getCostMaterials()
    {
        return this.baseCostMaterials + this.baseCostMaterials * this.level;
    }
	
    @Override
    public int getCostGold()
    {
        return this.baseCostGold + this.baseCostGold * this.level;
    }
    
    @Override
    public int getCostSP()
    {
        return this.baseCostSP + this.baseCostSP * this.level;
    }
    
    public double getSpace()
	{
		return this.space;
	}
    
    @Override
	public boolean removeWorkers(int amount)
	{
		return false;
	}
    
	private void updateSpace()
	{
		this.space = 20.0 + this.level * 10.0;
	}
}
