package ggj2016.model;

/**
* Class for managing a position in a 2-dimensional space
* @author Les Adeptes de Mordak
*/
public class Position
{
    private double x;
    private double y;

    /**
    * Constructor of a position with the coordinates given.
    * @param x Coordinate on X axis
    * @param y Coordinate on Y axis
    */
    public Position(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    /**
    * Constructor of a position without parameters, set the position to the origin.
    */
    public Position()
    {
        this(0,0);
    }
    
    /**
    * Add a position to the actual position
    * @param pos   position to add
    */
    public void addTo(Position pos)
    {
        this.x = this.x+pos.getX();
        this.y = this.y+pos.getY();
    }

    /**
    * Return the Euclidian distance to the position given in parameter
    * @param pos   position from which calculate the distance
    * @return      distance to the position
    */
    public double EuclidianDistanceTo(Position pos)
    {
        return Math.sqrt(SquaredEuclidianDistanceTo(pos));
    }
    
    /**
    * Provides the value of the attribute x.
    * @return the x value of the position
    */
    public double getX()
    {
        return this.x;
    }

    /**
    * Provides the value of the attribute y.
    * @return the value y of the position
    */
    public double getY()
    {
        return this.y;
    }
    
    /**
    * Return the Manhattan distance to the position given in parameter
    * @param pos   position from which calculate the distance
    * @return      distance to the position
    */
    public double ManhattanDistanceTo(Position pos)
    {
        return (Math.abs(pos.getX()-this.x)+Math.abs(pos.getY()-this.y));
    }
    
    /**
    *  Sets the coordinates of a position
    *  @param x coordinate
    *  @param y coordinate
    */
    public void setPosition(double x, double y)
    {
        this.setX(x);
        this.setY(y);
    }
    
    /**
    * Sets the value of the attribute x.
    * @param x new value for the attribute x
    */
    public void setX(double x)
    {
        this.x = x;
    }
    
    /**
    * Sets the value of the attribute y.
    * @param y new value for the attribute y
    */
    public void setY(double y)
    {
        this.y = y;
    }
    
    /**
    * Return the square of the Euclidian distance to the position given in parameter
    * @param pos   position from which calculate the distance
    * @return      square of the distance to the position
    */
    public double SquaredEuclidianDistanceTo(Position pos)
    {
        return (Math.pow(pos.getX()-this.x, 2)+Math.pow(pos.getY()-this.y,2));
    }
}
