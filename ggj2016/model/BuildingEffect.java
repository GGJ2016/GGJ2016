package ggj2016.model;

/*
* Effect executed by an event
*/
public class BuildingEffect extends Effect
{
    private final int level;
    
    /**
    *  Constructor
    *  @param game affected by this effect
    *  @param level to gain or to lose for each level
    */
    public BuildingEffect(Game game, int level)
    {
        super(game);
        this.level = level;
    }
   
    @Override
    public void execute()
    {
        Building[]  buildings = this.game.getBuildings();
	
        for(Building b : buildings)
	    {
            b.editLevel(this.level);
	    }
    }
}
