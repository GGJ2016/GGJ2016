package ggj2016.model;
import java.util.ArrayList;

/**
*  Utilitary class used to load and lauch Events in the game
*/
public class EventLauncher
{
    protected ArrayList<Event> events;
    protected Game game;
    
    /**
    *  @param game where the events occurs
    */
    public EventLauncher(Game game)
    {
        this.events = new ArrayList<>();
        this.game = game;
        this.load();
    }
    
    
    /**
    *  @param index
    *  @return the index-nth event
    */
    public Event getEvent(int index)
    {
        return this.events.get(index);
    }
    
    /**
    *  @return all the events of the game
    */
    public ArrayList<Event> getEvents()
    {
        return this.events;
    }

    public Event getTaxEvent(double tax)
    {
        Event ev = new Event("Tax", "You are asked to give "+(int)tax+" of each resource to the cult of Mordak", new String[]{"I'll pay !"});
        ev.addEffect(new ResourceEffect(this.game, 0/*t*/, 0/*p*/, -tax/*g*/, -tax/*m*/, -tax/*s*/));

        return ev;
    }
    
    /**
    *  load the events of the game
    */
    private void load()
    {
        // minute 0
        {
            Event ev = new Event("Welcome !", "From time to time, events will occur, and you will have to pay taxes. Have fun !", new String[]{"Let's go !"});
            this.events.add(ev);
        }
        // minute 1
        {
            Event ev = getTaxEvent(50);
            this.events.add(ev);
	    
            Event ev2 = new Event("", "A wave of fear hits the village, the wind whispers Mordak's name", new String[]{"You make an offering to Mordak", "You make a speech to reassure your people."});
            ev.addReaction(ev2);
	    
            Event ev2a = new Event("Effect : Gold -50 ; Trust -5", "Mordak asks you to summon him", new String[]{"You swear to do it, in front of the whole village", "You wait until you are alone, then you swear to do it"});
            ev2a.addEffect(new ResourceEffect(this.game, -5, 0, -50, 0, 0));
            ev2.addReaction(ev2a);
	    
            Event ev2a1 = new Event("Summon Points +60 ; Trust -5", "People stare at you");
            ev2a1.addEffect(new ResourceEffect(this.game, -5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 60/*s*/));
	    
            Event ev2a2 = new Event("Trust +10", " Mordak thanks you.");
            ev2a2.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
	    
            ev2a.addReaction(ev2a1);
            ev2a.addReaction(ev2a2);
	    
            Event ev2b = new Event("Materials +20 ; Trust + 5", "Mordak visits you in a dream and asks you to summon him", new String[]{"You swear allegiance to him", "You deny his request"});
            ev2b.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 0/*g*/, 20/*m*/, 0/*s*/));
            ev2.addReaction(ev2b);

            Event ev2b1 = new Event("Summon Points +20. Trust +5", "He thanks you.");
            ev2b1.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 20/*s*/));
            Event ev2b2 = new Event("Summon Points +20. Trust -5", "You wake up, soaked with sweat, somehow you have to summon Mordak, you need to...");
            ev2b2.addEffect(new ResourceEffect(this.game, -5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 20/*s*/));
            ev2b.addReaction(ev2b1);
            ev2b.addReaction(ev2b2);
        }
        // minute 2
        {
            Event ev = getTaxEvent(100);
            this.events.add(ev);

            Event ev1 = new Event("", "A strange hooded man comes to see you. Without a word, he hands you a book. On it, a pentacle is drawn", new String[]{"You take the book", "You step backward as you see the pentacle"});
            ev.addReaction(ev1);
            // take the book
            Event ev1a = new Event("Trust -5. Summon Points +50", "The man whispers : \"You will need that to summon our master...\". He then disappear in a cloud of smokes. As the smoke clears up, you hear echoes of laughter, then silence...");
            ev1a.addEffect(new ResourceEffect(this.game, -5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 50/*s*/));
            ev1.addReaction(ev1a);
            // step backward
            Event ev1b = new Event("Materials +50", "The man puts the book on a shelf, and whispers : \"You will find this very useful, in order to acheive your plans...\"", new String[]{"You nod, staring at the bookshelf.", "You call for guards"});
            ev1b.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 0/*g*/, 50/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
            // nod
            Event ev1ba = new Event("Trust +3", "he man goes away. You don't seem to remember even the smallest detail about him. Was he really here ? How did that book get there ?");
            ev1ba.addEffect(new ResourceEffect(this.game, 3/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1b.addReaction(ev1ba);
            // call
            Event ev1bb = new Event("Trust -10. Population -2", "The man disappear in a burst of flames, killing the two guards how came to help you. Your face takes a hit.");
            ev1ba.addEffect(new ResourceEffect(this.game, -10/*t*/, -2/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1b.addReaction(ev1bb);
        }
        // minute 3
        {
            Event ev = getTaxEvent(150);
            this.events.add(ev);

            Event ev1 = new Event("Population -1. Trust -2", "A minor demon infiltrates the village and assassinates a man.");
            ev1.addEffect(new ResourceEffect(this.game, -2/*t*/, -1/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev.addReaction(ev1);
        }
        // minute 4
        {
            Event ev = getTaxEvent(200);
            this.events.add(ev);

            Event ev1 = new Event("", "Rumors about demons attracted an exorcist in the village.", new String[]{"You kill him", "Despite your reluctance, you let him exorcize the village."});
            ev.addReaction(ev1);
            // kill
            Event ev1a = new Event("Trust -15.", "The rebellion starts to brew in the village.");
            ev1a.addEffect(new ResourceEffect(this.game, -15/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1a);
            // exorcize
            Event ev1b = new Event("Trust +10 ; Gold +100 ; Materials +100", "He feels a demonic aura emmanating from you.", new String[]{"You kill him right away.", "You wait beeing left alone with him to kill him."});
            ev1b.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, 100/*g*/, 100/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
            // kill right away
            Event ev1ba = new Event("Trust -25", "A riot starts in the village.");
            ev1ba.addEffect(new ResourceEffect(this.game, -25/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1b.addReaction(ev1ba);
            // wait and kill
            Event ev1bb = new Event("Gold +200. Trust +5.", "He dies silently");
            ev1bb.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 200/*g*/, 0/*m*/, 0/*s*/));
            ev1b.addReaction(ev1bb);
        }
        // minute 5
        {
            Event ev = getTaxEvent(250);
            this.events.add(ev);

            Event ev1 = new Event("Population -20. Trust -5.", "A two headed calf was born. The villagers panic and some of the inhabitants run away.");
            ev1.addEffect(new ResourceEffect(this.game, -5/*t*/, -20/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev.addReaction(ev1);
        }
        // minute 6
        {
            Event ev = getTaxEvent(300);
            this.events.add(ev);

            Event ev1 = new Event("", "Some of your relatives steals you 200 gold and flee...", new String[]{"Mordak gives you the control over a minor demon to catch them.", "You send the guards after the thieves."});
            ev.addReaction(ev1);
            // demon
            Event ev1a = new Event("Gold +350. Trust -10.", "Some half devoured bloody parts of the thieves are found.");
            ev1a.addEffect(new ResourceEffect(this.game, -10/*t*/, 0/*p*/, 350/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1a);
            // guards
            Event ev1b = new Event("Population -5. Trust +5.", "The guards never come back.");
            ev1b.addEffect(new ResourceEffect(this.game, 5/*t*/, -5/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
        }
        // minute 7
        {
            Event ev = getTaxEvent(350);
            this.events.add(ev);

            Event ev1 = new Event("Materials +500. Gold +500.","A talented smith join the village");
            ev1.addEffect(new ResourceEffect(this.game, 0/*t*/, 0/*p*/, 500/*g*/, 500/*m*/, 0/*s*/));
            ev.addReaction(ev1);
        }
        // minute 8
        {
            Event ev = getTaxEvent(350);
            this.events.add(ev);

            Event ev1 = new Event("", "Mordak threat you to burn the village if you don't summon him quickly.", new String[]{"You perform a human sacrifice to calm him.", "You offer Mordak some gold."});
            ev.addReaction(ev1);
            // sacrifice
            Event ev1a = new Event("Population -2. Trust -5.", "Mordak give you a power gem.", new String[]{"You use it.", "You store it."});
            ev1a.addEffect(new ResourceEffect(this.game, -5/*t*/, -2/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1a);
            // use
            Event ev1aa = new Event("Summon points +750. Trust -5.", "");
            ev1aa.addEffect(new ResourceEffect(this.game, -5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 750/*s*/));
            ev1a.addReaction(ev1aa);
            // store
            Event ev1ab = new Event("Trust +10. Materials +400.", "The gem disepear few days latter.");
            ev1ab.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, 0/*g*/, 400/*m*/, 0/*s*/));
            ev1a.addReaction(ev1ab);
            //gift
            Event ev1b = new Event("Gold -100. All the buildins loose one level. Summon points +200. Trust +10", "");
            ev1b.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, -100/*g*/, 0/*m*/, 200/*s*/));
            ev1b.addEffect(new BuildingEffect(this.game, -1));
            ev1.addReaction(ev1b);
        }
        // minute 9
        {
            Event ev = getTaxEvent(350);
            this.events.add(ev);

            Event ev1 = new Event("", "Mordak try to convert some other villagers to venerate him.", new String[]{"You perform a secret meeting with them and explain them what your trying to do.", "You meet them one by one to explain them your plan."});
            ev.addReaction(ev1);
            // meeting
            Event ev1a = new Event("Trust +2. Summon points +1000.", "They swear to help you");
            ev1a.addEffect(new ResourceEffect(this.game, 2/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 1000/*s*/));
            ev1.addReaction(ev1a);

            Event ev1a2 = new Event("", "One of them was a traitor and he reveals your plan to the villagers.", new String[]{"You kill him.", "You blame him to be a fool."});
            ev1a.addReaction(ev1a2);
            // kill
            Event ev1a2a = new Event("Trust -30. Gold -500. Materials -500. Summon points -500.", "");
            ev1a2a.addEffect(new ResourceEffect(this.game, -30/*t*/, 0/*p*/, -500/*g*/, -500/*m*/, -500/*s*/));
            ev1a2.addReaction(ev1a2a);
            // fool
            Event ev1a2b = new Event("Trust +10. Gold +250. Summon points +250.", "Your adepts help you to make him look silly.");
            ev1a2b.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, 250/*g*/, 0/*m*/, 250/*s*/));
            ev1a.addReaction(ev1a2b);
            //explain
            Event ev1b = new Event("Trust +15. Summon points +200. Gold +300", "You expose one of them to be a traitor, and you kill him, and make it looks like an accident.");
            ev1b.addEffect(new ResourceEffect(this.game, 15/*t*/, 0/*p*/, 300/*g*/, 0/*m*/, 200/*s*/));
            ev1.addReaction(ev1b);
        }
        // minute 10
        {
            Event ev = getTaxEvent(400);
            this.events.add(ev);

            Event ev1 = new Event("", "An itinerant trader pass throught the village and try to sell you a statuette.", new String[]{"You buy it.", "You force him off the village."});
            ev.addReaction(ev1);
            // buy
            Event ev1a = new Event("", "It's cursed !", new String[]{"You throw it away.", "You sacrifice it to Mordak."});
            ev1.addReaction(ev1a);
            // throw it
            Event ev1aa = new Event("Population -20. Trust -20.", "");
            ev1aa.addEffect(new ResourceEffect(this.game, -20/*t*/, -20/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1a.addReaction(ev1aa);
            // sacrifice it
            Event ev1ab = new Event("Summon points +500", "");
            ev1ab.addEffect(new ResourceEffect(this.game, 0/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 500/*s*/));
            ev1a.addReaction(ev1ab);
            // force
            Event ev1b = new Event("Gold -250. Materials -250", "");
            ev1b.addEffect(new ResourceEffect(this.game, 0/*t*/, 0/*p*/, -250/*g*/, -250/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
        }
        // minute 11
        {
            Event ev = getTaxEvent(450);
            this.events.add(ev);

            Event ev1 = new Event("", "A virgin get pregnant and give birth to a demon.", new String[]{"You burn them alive.", "You sacrifice the demon to Mordak."});
            ev.addReaction(ev1);
            // burn
            Event ev1a = new Event("Trust -10. Summon points -200.", "The demon eat the flames, grows up and ravage the village.", new String[]{"You ask Mordak to bannish it.", "You let your soldiers take care of it."});
            ev1a.addEffect(new ResourceEffect(this.game, -10/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, -200/*s*/));
            ev1.addReaction(ev1a);
            // > banish
            Event ev1aa = new Event("Trust -10. Summon points -200.", "");
            ev1aa.addEffect(new ResourceEffect(this.game, -10/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, -200/*s*/));
            ev1a.addReaction(ev1aa);
            // > soldiers
            Event ev1ab = new Event("Population -15. Trust +5.", "");
            ev1ab.addEffect(new ResourceEffect(this.game, 5/*t*/, -15/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1a.addReaction(ev1ab);
            // sacrifice
            Event ev1b = new Event("Trust +5. Summon points +100.", "Mordak ask you to sacrifice the mother.", new String[]{"You accept.", "You decline"});
            ev1b.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 100/*s*/));
            ev1.addReaction(ev1b);
            // > accept
            Event ev1ba = new Event("Population -3. Summon points -200. Trust -10.", "");
            ev1ba.addEffect(new ResourceEffect(this.game, -10/*t*/, -3/*p*/, 0/*g*/, 0/*m*/, -200/*s*/));
            ev1b.addReaction(ev1ba);
            // > decline
            Event ev1bb = new Event("Trust +10. Materials +200.", "Mordak is furious. All the buildings loose a level.");
            ev1bb.addEffect(new ResourceEffect(this.game, 10/*t*/, -3/*p*/, 0/*g*/, 200/*m*/, 0/*s*/));
            ev1b.addReaction(ev1bb);
        }
        // minute 12
        {
            Event ev = getTaxEvent(500);
            this.events.add(ev);

            Event ev1 = new Event("Gold +1000", "Mordak give you some gold to help you summon him.");
            ev1.addEffect(new ResourceEffect(this.game, 0/*t*/, 0/*p*/, 1000/*g*/, 0/*m*/, 0/*s*/));
            ev.addReaction(ev1);
        }
        // minute 13
        {
            Event ev = getTaxEvent(500);
            this.events.add(ev);

            Event ev1 = new Event("", "The corruption of Mordak grows through the village.", new String[]{"You let it grow.", "You try to contain it."});
            ev.addReaction(ev1);

            // grow
            Event ev1a = new Event("Trust +5.", "You are victim of an assassination attempt.", new String[]{"You beg Mordak to help you.", "You defend yourself alone."});
            ev1a.addEffect(new ResourceEffect(this.game, 5/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1a);
            // > beg
            Event ev1aa = new Event("Trust -5. Population -10.", "");
            ev1aa.addEffect(new ResourceEffect(this.game, -5/*t*/, -10/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1a.addReaction(ev1aa);
            // > alone
            Event ev1ab = new Event("Trust +5. Population -10. Materials -200.", "");
            ev1ab.addEffect(new ResourceEffect(this.game, 5/*t*/, -10/*p*/, 0/*g*/, -200/*m*/, 0/*s*/));
            ev1a.addReaction(ev1ab);
            // contain
            Event ev1b = new Event("Trust -10. Population -10. Gold -100.", "Mordak shows itself and hurt you, living a big scar on your face.");
            ev1b.addEffect(new ResourceEffect(this.game, -10/*t*/, -10/*p*/, -100/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
        }
        // minute 14
        {
            Event ev = getTaxEvent(500);
            this.events.add(ev);

            Event ev1 = new Event("", "Mordak become more and more impatient.", new String[]{"You sacrifice some virgins to calm him.", "You ignore him."});
            ev.addReaction(ev1);
            // virgins
            Event ev1a = new Event("Trust -20. Summon points +1500.", "");
            ev1a.addEffect(new ResourceEffect(this.game, -20/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 1500/*s*/));
            ev1.addReaction(ev1a);
            // ignore
            Event ev1b = new Event("Trust +10", "");
            ev1b.addEffect(new ResourceEffect(this.game, 10/*t*/, 0/*p*/, 0/*g*/, 0/*m*/, 0/*s*/));
            ev1.addReaction(ev1b);
        }
        // minute 15
        {
            Event ev = new Event("The End.", "Mordak get mad at your incompetence and finally kills you.");
            this.events.add(ev);
        }
    }
}
