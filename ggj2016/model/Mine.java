package ggj2016.model;

public class Mine extends BuildingResource
{
    public Mine(Population pop)
    {
        super(pop);
        
        this.level = 0;
        this.name = "Mine";
        this.position = new Position(950.0, 5.0);
        this.currentWorkers = 0;
        this.maxWorkers = 0;
        this.currentIncome = 0.;
        this.baseIncome = 0.7;

        this.setBaseCosts(400, 500, 0);
    }
}
