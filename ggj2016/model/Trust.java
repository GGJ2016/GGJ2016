package ggj2016.model;

public class Trust extends Resource
{
    public Trust()
    {
      this.amount = 0;
    }
  
    public void spend(int amount)
    {
      if(hasEnough(amount))
      {
        this.amount -= amount;
      }
      else
      {
        Log.write("Not enough trust"); // TODO meilleur message d'erreur
      }
    }
}
