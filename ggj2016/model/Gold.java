package ggj2016.model;

public class Gold extends Resource
{
    public Gold()
    {
      this.amount = 0;
    }
  
    public void spend(int amount)
    {
        if (hasEnough(amount) )
        {
            this.amount -= amount;
        }
        else
        {
            Log.write("Not enough gold"); // TODO meilleur message d'erreur
        }
    }
}
