package ggj2016.model;

public class Graveyard extends BuildingResource
{
	public Graveyard(Population pop)
	{
		super(pop);

		this.level = 0;
		this.name = "Graveyard";
		this.position = new Position(875., 250.);
		this.currentWorkers = 0;
		this.maxWorkers = 0;
		this.currentIncome = 0.;
		this.baseIncome = 0.4;
		
		this.setBaseCosts(250, 250, 600); // TODO edit ?
	}

    @Override
    public int getCostGold()
    {
        if(this.level == 0)
        {
            return 1000;
        }
        
        return super.getCostGold();
    }
      
    @Override
    public int getCostMaterials()
    {
        if(this.level == 0)
        {
            return 1000;
        }
        
        return super.getCostMaterials();
    }
      
    @Override
    public int getCostSP()
    {
        if(this.level == 0)
        {
            return 0;
        }
      
        return super.getCostMaterials();
    }	
}
