The Call Of Mordak
==================
Game made during the Global Game Jam 2016

Team
----
BoG
Diarmund
LLoyd_Angifer
Jeff
Shadowar
Solias
Van

Thanks
------
Thanks to all the workers ont this Global GameJame 2016

Special Thanks
--------------
A special thank to all the partenaires of GameConcoillotte Team for this event and their organization !

Resources and licences
======================

Images we used:
---------------
Icones by Van
Others by Shad0war
Licence: CC-BY-3.0

Sounds we used
--------------

* sounds/roar.wav :
		Author: qubodup
		Find it here http://opengameart.org/content/big-monster-moan >
		Licence: CC-BY-SA 3.0 / GPL 3.0 / GPL 2.0

* sounds/win.wav && sounds/Haha.wav:
	       Author: TKZ Productions
	       Find it here : http://opengameart.org/content/epic-man-of-death-voice
	       Licence: CC-BY 3.0


Musics we used
--------------

* musics/lose.wav :
		Author: tgfcoder
		Find it here http://opengameart.org/content/her-violet-eyes
		Licence: CC-BY-SA 3.0

* musics/game.wav
	    Author : unknow:
	    Licence: CC-BY-SA 3.0
* opening.wav
	    Author : unknow:
	    Licence: CC-BY-SA 3.0
* victory.wav :
	    Author : unknow:
	    Licence: CC-BY-SA 3.0

