GAME=GGJ2016
PROJECT=ggj2016
SRC=$(wildcard *.java) $(wildcard $(PROJECT)/controller/*.java) $(wildcard $(PROJECT)/model/*.java) $(wildcard $(PROJECT)/view/*.java)
CLASS=`find . -name "*.class"`
DATA=assets

all:
	javac $(SRC)

run:all
	java $(GAME)

clean:
	rm -rf $(CLASS)

mrproper:clean
	rm -rf $(GAME).jar

jar:all
	jar -cvfe $(GAME).jar $(GAME) $(CLASS) $(DATA)

