import ggj2016.controller.*;
import ggj2016.view.*;
import ggj2016.model.*;

/**
 * Main class for test the Gameskel
 * 
 * @author Virgil Manrique
 */
public class GGJ2016
{
	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args)
	{
		Model model = new Model();
		View view = new View(model);
		Controller controller = new Controller(view, model);
		controller.run();
	}
}
